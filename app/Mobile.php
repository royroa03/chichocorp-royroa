<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;


class Mobile
{

  /**
   * @var CarrierInterface
   */

	protected $provider;
	
	function __construct(/*CarrierInterface $provider*/)
	{
		/*$this->provider = $provider;*/
	}


	public function makeCallByName($name = '')
	{
		if( empty($name) ) return;

		$contact = ContactService::findByName($name);

		$this->provider->dialContact($contact);

		return $this->provider->makeCall();
	}

  public function dialContact($name)
  {
    $result = [];

    $data_user = array(
        array(
            "name" => "Roy Roa 1",
            "phone" => "917919001"
        ),
        array(
            "name" => "Roy Roa 2",
            "phone" => "917919002"
        ),
        array(
            "name" => "Roy Roa 3",
            "phone" => "917919003"
        )
    );

    foreach ($data_user as $value) {
      if($value['name'] == $name){
        $result['name'] = $value['name'];
        $result['phone'] = $value['phone'];
      }
    }

    return $result;

  }


}
