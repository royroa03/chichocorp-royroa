<?php

namespace App\Services;

use App\Contact;


class ContactService
{
	
	public static function findByName(string $name): Contact
	{
	  $obj_contact = new Contact();

	  echo json_encode($obj_contact->findByName($name));

	}

	public static function validateNumber(string $number): bool
	{
		// logic to validate numbers
    $reg = "#^\(?\d{2}\)?[\s\.-]?\d{4}[\s\.-]?\d{4}$#";
    return preg_match($reg, $number);
	}
}