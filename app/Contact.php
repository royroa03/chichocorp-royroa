<?php

namespace App;


class Contact
{
	
	function __construct()
	{
		# code...
	}

  /**
   * @var string
   */
  public $name;

  /**
   * @var string
   */
  public $phone;

  /**
   * @var string
   */
  protected $table = 'contact';

  /**
   * @var array
   */
  protected $fillable = [
      'id',
      'name',
      'phone',
  ];

  /**
   *
   */
  public function findByName($name)
  {
    $result = [];

    $data_user = array(
        array(
            "name" => "Roy Roa 1",
            "phone" => "917919001"
        ),
        array(
            "name" => "Roy Roa 2",
            "phone" => "917919002"
        ),
        array(
            "name" => "Roy Roa 3",
            "phone" => "917919003"
        )
    );

    foreach ($data_user as $value) {
      if($value['name'] == $name){
        $result['name'] = $value['name'];
        $result['phone'] = $value['phone'];
      }
    }

    return $result;

  }

}